const express = require("express");
const route = express.Router();
const { HistoryController } = require("../controllers");

route.get("/transaksi", HistoryController.HistoryTransaksi);
route.get(
  "/barang/:jenis/:no_penjualan",
  HistoryController.HistoryTransaksiByNo
);

route.get(
  "/transaksi/:no_penjualan",
  HistoryController.GetTransaksiByNoPenjualan
);

module.exports = route;
