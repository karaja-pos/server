const express = require("express");
const route = express.Router();
const { uploadStok } = require("../utils/uploads");
const { FileController } = require("../controllers");

// route.post(`/stok`, FileController.UploadStok);
route.post(`/stok`, uploadStok.single("stok"), FileController.UploadStok);

module.exports = route;
