const express = require("express");
const route = express.Router();
const { UsersController } = require("../controllers");

route.get("/", UsersController.GetUsers);
route.post("/", UsersController.UserRegister);
route.post("/login", UsersController.UserLogin);

module.exports = route;
