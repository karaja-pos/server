const express = require("express");
const fileUpload = require("express-fileupload");
const route = express.Router();
const { KaryawanController } = require("../controllers");

route.use(fileUpload());

route.get("/", KaryawanController.GetAllKaryawan);
// route.get("/toko/:id", KaryawanController.GetKaryawanByIdToko);
route.get("/:id_toko", KaryawanController.GetKaryawanByIdToko);
route.get("/posisi/:id_posisi", KaryawanController.GetKaryawanByIdPosisi);
route.post("/", KaryawanController.CreatedKaryawan);
route.put("/blokir/:id", KaryawanController.BlokirKaryawan);
route.put("/:id", KaryawanController.UpdateKaryawan);

module.exports = route;
