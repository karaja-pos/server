const express = require("express");
const route = express.Router();
const { KrediturController } = require("../controllers");

route.get("/", KrediturController.GetAllKreditur);
route.post("/", KrediturController.CreateKreditur);
route.put("/:id", KrediturController.UpdateKrediturById);
route.put("/status/:id", KrediturController.UpdateStatusKreditur);

module.exports = route;
