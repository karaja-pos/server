const express = require("express");
const route = express.Router();
const { KasirController } = require("../controllers");

route.get("/:id_toko", KasirController.GetBarangToko);
route.post("/login", KasirController.KasirLogin);
route.post("/logout", KasirController.KasirLogout);
route.post("/", KasirController.TransaksiKasir);

module.exports = route;
