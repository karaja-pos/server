const express = require("express");
const route = express.Router();
const { PenarikanController } = require("../controllers");

route.get("/all", PenarikanController.GetAllPenarikan);
route.get("/toko/:id_toko", PenarikanController.GetPenarikanByToko);
route.get("/store/:id_toko", PenarikanController.GetToko);
route.get("/count/:id_toko", PenarikanController.GetCountPenarikan);
route.put("/baca/:id", PenarikanController.Baca);
route.put("/tolak/:id", PenarikanController.HandleTolak);
route.put("/setuju/:id", PenarikanController.HandleSetuju);
route.post("/", PenarikanController.CreatePenarikan);

module.exports = route;
