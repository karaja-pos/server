const express = require("express");
const route = express.Router();
const { TokoController } = require("../controllers");

route.get("/", TokoController.GetAllToko);
route.post("/", TokoController.CreateToko);
route.put("/:id", TokoController.UpdateTokoById);
route.put("/status/:id", TokoController.UpdateStatusToko);

module.exports = route;
