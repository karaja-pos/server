const express = require("express");
const route = express.Router();
const { SatuanController } = require("../controllers");

route.get("/", SatuanController.GetAllSatuan);
route.post("/", SatuanController.CreateSatuan);
route.put("/:id", SatuanController.UpdateSatuan);
route.put("/status/:id", SatuanController.SetActive);

module.exports = route;
