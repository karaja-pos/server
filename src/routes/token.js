const express = require("express");
const route = express.Router();
const { TokenController } = require("../controllers");

route.get("/:token", TokenController.CheckToken);
route.get("/all/data", TokenController.GetAllToken);

module.exports = route;
