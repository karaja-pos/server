const express = require("express");
const route = express.Router();
const { KategoriController } = require("../controllers");

route.get("/", KategoriController.GetAllKategori);
route.post("/", KategoriController.CreateKategori);
route.put("/:id", KategoriController.UpdateKategori);
route.put("/status/:id", KategoriController.SetStatus);

module.exports = route;
