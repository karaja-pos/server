const express = require("express");
const route = express.Router();
const { PosisiController } = require("../controllers");

route.get("/", PosisiController.GetAllPosisi);
route.post("/", PosisiController.CreatePosisi);
route.put("/:id", PosisiController.UpdatePosisi);
route.put("/blokir/:id", PosisiController.BlokirPosisi);

module.exports = route;
