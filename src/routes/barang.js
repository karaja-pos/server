const express = require("express");
const route = express.Router();
const { BarangController } = require("../controllers");
const { uploadBarang } = require("../utils/uploads");

route.get("/", BarangController.GetSemuaBarang);
route.get("/toko/:id", BarangController.GetStokTokoById);
route.get("/barang-toko/:id", BarangController.GetBarangByToko);
route.get("/:code", BarangController.GetBarangByCode);
route.post("/", BarangController.CreateBarang);
route.post("/to-toko/:id", BarangController.InsertDataInStokToko);
route.put("/:id", uploadBarang.single("file"), BarangController.UpdateBarang);
route.put("/harga-jual/:id", BarangController.SetTotalHarga);
route.put("/stok-akhir/:id", BarangController.SetStokBarang);
route.put("/diskon/:id", BarangController.SetDiskonBarang);
route.put("/status/stok", BarangController.UpdateStatusStok);

module.exports = route;
