const express = require("express");
const route = express.Router();

route.use("/users", require("./users"));
route.use("/barang", require("./barang"));
route.use("/karyawan", require("./karyawan"));
route.use("/kategori", require("./kategori"));
route.use("/merek", require("./merek"));
route.use("/posisi", require("./posisi"));
route.use("/toko", require("./toko"));
route.use("/supplier", require("./suplier"));
route.use("/satuan", require("./satuan"));
route.use("/kreditur", require("./kreditur"));
route.use("/gudang", require("./gudang"));
route.use("/kasir", require("./kasir"));
route.use("/info", require("./info"));
route.use("/history", require("./history"));
route.use("/token", require("./token"));
route.use("/laporan", require("./laporan"));
route.use("/penarikan", require("./penarikan"));
route.use("/file", require("./file"));

module.exports = route;
