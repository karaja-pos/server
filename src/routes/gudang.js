const express = require("express");
const route = express.Router();
const { GudangController } = require("../controllers");

// pembelian
route.get("/", GudangController.GetFakturPembelian);
route.get("/faktur/:nomor", GudangController.GetFakturByNomor);
route.post("/faktur", GudangController.AddFakturPembelian);
route.delete("/faktur/:no_faktur", GudangController.DeleteFakturById);

// pengiriman
route.get("/pengiriman", GudangController.GetPengiriman);
route.get("/pengiriman/history", GudangController.GetHistoryPengiriman);
route.get("/pengiriman/:id_toko", GudangController.GetPengirimanByToko);
route.get("/pengiriman/cetak/:no_pengiriman", GudangController.CetakPengiriman);
route.get(
  "/pengiriman/items/:no_pengiriman",
  GudangController.GetPengirimanItems
);
route.post("/pengiriman", GudangController.KirimBarang);
route.delete("/pengiriman/:id", GudangController.HapusPengiriman);
route.put("/pengiriman/toko/:id_toko", GudangController.TerimaPengirimanByToko);

module.exports = route;
