const express = require("express");
const route = express.Router();
const { InfoController } = require("../controllers");

route.get("/kasir-aktif", InfoController.KasirAktif);
// route.get("/total-penjualan", InfoController)
// route.get("/penjualan-kasir", InfoController)
// route.get("/trafik-penjualan", InfoController)

module.exports = route;
