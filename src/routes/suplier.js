const express = require("express");
const route = express.Router();
const { SuplierController } = require("../controllers");

route.get("/", SuplierController.GetAllSuplier);
route.post("/", SuplierController.CreateSuplier);
route.put("/:id", SuplierController.UpdateSupplier);
route.put("/blokir/:id", SuplierController.BlokirSupplier);

module.exports = route;
