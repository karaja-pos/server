const express = require("express");
const route = express.Router();
const { LaporanController } = require("../controllers");

route.get("/", LaporanController.GetLaporanPenjualanTunai);
route.get("/tunai", LaporanController.GetLaporanTunai);
route.get("/nontunai", LaporanController.GetLaporanNonTunai);
route.get("/kredit", LaporanController.GetLaporanKredit);

module.exports = route;
