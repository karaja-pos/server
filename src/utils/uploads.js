const moment = require("moment");
const multer = require("multer");

const storageBarang = multer.diskStorage({
  destination: function (req, file, done) {
    console.log(__dirname);
    done(null, "src/public/uploads/barang/");
  },

  filename: function (req, file, done) {
    done(null, `${moment().format("yyyyMMDD_HHmmss")}_${file.originalname}`);
  },
});

const storageStok = multer.diskStorage({
  destination: function (req, file, done) {
    console.log(__dirname);
    done(null, "src/public/uploads/stok/");
  },

  filename: function (req, file, done) {
    done(null, `${moment().format("yyyyMMDD_HHmmss")}_${file.originalname}`);
  },
});

const storageProfile = multer.diskStorage({
  destination: function (req, file, done) {
    console.log(__dirname);
    done(null, "src/public/uploads/profile/");
  },

  filename: function (req, file, done) {
    done(null, `${moment().format("yyyyMMDD_HHmmss")}_${file.originalname}`);
  },
});

const filterFile = (req, file, done) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    done(null, true);
  } else {
    done(null, false);
  }
};

// const filterFile = (req, file, done) => {
//   if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//     done(null, true);
//   } else {
//     done(null, false);
//   }
// };

// const uploadPengaduan = multer({
//   //   dest: "src/public/uploads/",
//   storage: storagePengaduan,
//   limits: {
//     fileSize: 1024 * 1024 * 10,
//   },
//   fileFilter: filterFile,
// });

const uploadProfile = multer({
  //   dest: "src/public/uploads/",
  storage: storageProfile,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  fileFilter: filterFile,
});

const uploadStok = multer({
  //   dest: "src/public/uploads/",
  storage: storageStok,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  // fileFilter: filterFile,
});

const uploadBarang = multer({
  //   dest: "src/public/uploads/",
  storage: storageBarang,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  fileFilter: filterFile,
});

module.exports = {
  // uploadPengaduan,
  uploadProfile,
  uploadStok,
  uploadBarang,
};
