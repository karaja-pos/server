const bcrypt = require("bcryptjs");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET SEMUA KARYAWAN
const GetAllKaryawan = async (req, res, next) => {
  try {
    const data = await db(tableName.karyawan)
      .select(
        `${tableName.karyawan}.id`,
        `${tableName.karyawan}.kode_karyawan`,
        `${tableName.karyawan}.nama_karyawan`,
        `${tableName.karyawan}.nomor_hp`,
        `${tableName.karyawan}.gambar_karyawan`,
        `${tableName.karyawan}.id_posisi`,
        `${tableName.karyawan}.id_toko`,
        `${tableName.karyawan}.id_gambar`,
        `${tableName.karyawan}.status`,
        `${tableName.karyawan}.blokir`,
        `${tableName.posisi}.posisi`,
        `${tableName.toko}.nama_toko`,
        `${tableName.file}.type`,
        `${tableName.file}.file`
      )
      .leftJoin(
        tableName.toko,
        `${tableName.karyawan}.id_toko`,
        `${tableName.toko}.id`
      )
      .leftJoin(
        tableName.posisi,
        `${tableName.karyawan}.id_posisi`,
        `${tableName.posisi}.id`
      )
      .leftJoin(
        tableName.file,
        `${tableName.karyawan}.id_gambar`,
        `${tableName.file}.id`
      );

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// GET karyawan by id toko
const GetKaryawanByIdToko = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.karyawan)
      .select(
        "kode_karyawan",
        "nama_karyawan",
        "gambar_karyawan",
        "id_posisi",
        "id_toko",
        "status"
      )
      .where({ id_toko });

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// GET karyawan by id posisi
const GetKaryawanByIdPosisi = async (req, res, next) => {
  const { id_posisi } = req.params;
  try {
    const data = await db(tableName.karyawan)
      .select(
        "kode_karyawan",
        "nama_karyawan",
        "gambar_karyawan",
        "id_posisi",
        "id_toko",
        "status"
      )
      .where({ id_posisi });

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED KARYAWAN
const CreatedKaryawan = async (req, res, next) => {
  const {
    kode_karyawan,
    nama_karyawan,
    id_posisi,
    id_toko,
    nomor_hp,
    user,
  } = req.body;
  try {
    const passwordHash = await bcrypt.hashSync(kode_karyawan, 12);
    // req.files !== null ? "" : req.files.file.data,
    if (req.files !== null) {
      const simpanFile = await db(tableName.file).insert({
        title: nama_karyawan,
        type: req.files.file.mimetype,
        file: req.files.file.data,
        user: "",
      });

      const addKaryawan = await db(tableName.karyawan).insert({
        kode_karyawan,
        password: passwordHash,
        nama_karyawan,
        nomor_hp,
        gambar_karyawan: "",
        id_gambar: simpanFile[0],
        id_posisi,
        id_toko,
        user,
      });

      await db(tableName.users).insert({
        email: nama_karyawan,
        username: kode_karyawan,
        password: passwordHash,
        role: "user",
        id_pengguna: addKaryawan[0],
        status: 1,
      });

      if (addKaryawan) {
        return WebResponse(res, 201, "Created", addKaryawan);
      }
    } else {
      const addKaryawan = await db(tableName.karyawan).insert({
        kode_karyawan,
        password: passwordHash,
        nama_karyawan,
        nomor_hp,
        gambar_karyawan: "",
        id_posisi,
        id_toko,
        user,
      });

      await db(tableName.users).insert({
        email: nama_karyawan,
        username: kode_karyawan.toLowerCase(),
        password: passwordHash,
        role: "user",
        id_pengguna: addKaryawan[0],
        status: 1,
      });

      if (addKaryawan) {
        return WebResponse(res, 201, "Created", addKaryawan);
      }
    }

    console.log(req.body);
  } catch (error) {
    return next(error);
  }
};

const UpdateKaryawan = async (req, res, next) => {
  const { id } = req.params;
  const {
    kode_karyawan,
    nama_karyawan,
    id_posisi,
    id_toko,
    nomor_hp,
    id_gambar,
  } = req.body;

  try {
    if (req.files !== null) {
      const simpanFile = await db(tableName.file).insert({
        title: nama_karyawan,
        type: req.files.file.mimetype,
        file: req.files.file.data,
        user: "",
      });

      const update = await db(tableName.karyawan)
        .update({
          kode_karyawan,
          nama_karyawan,
          nomor_hp,
          gambar_karyawan: "",
          id_gambar: simpanFile[0],
          id_posisi,
          id_toko,
        })
        .where({ id });

      if (update) {
        return WebResponse(res, 201, "Created", update);
      }
    } else {
      const update = await db(tableName.karyawan)
        .update({
          kode_karyawan,
          nama_karyawan,
          nomor_hp,
          gambar_karyawan: "",
          id_posisi,
          id_toko,
        })
        .where({ id });

      if (update) {
        return WebResponse(res, 201, "Created", update);
      }
    }
  } catch (error) {
    return next(error);
  }
};

const BlokirKaryawan = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.karyawan)
      .update({ blokir: status })
      .where({ id });
    req.io.sockets.emit("blokir-karyawan", id);
    return WebResponse(res, 201, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

const GetKaryawanByToko = async (req, res, next) => {
  const { id } = req.params;
  try {
    const data = await db(tableName.karyawan)
      .select("*")
      .where({ id_toko: id });
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetAllKaryawan,
  CreatedKaryawan,
  GetKaryawanByIdToko,
  GetKaryawanByIdPosisi,
  BlokirKaryawan,
  UpdateKaryawan,
  GetKaryawanByToko,
};
