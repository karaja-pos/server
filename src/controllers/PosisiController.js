const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL POSISI
const GetAllPosisi = async (req, res, next) => {
  try {
    const data = await db(tableName.posisi).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED POSISI
const CreatePosisi = async (req, res, next) => {
  const { posisi, user } = req.body;
  try {
    const addPosisi = await db(tableName.posisi).insert({
      posisi,
      user,
    });

    if (addPosisi) {
      return WebResponse(res, 201, "Created", addPosisi);
    }
  } catch (error) {
    return next(error);
  }
};

// Blokir posisi
const BlokirPosisi = async (req, res, next) => {
  const { id } = req.params;
  const { blokir } = req.body;
  try {
    const update = await db(tableName.posisi).update({ blokir }).where({ id });
    return WebResponse(res, 200, "Updated", blokir);
  } catch (error) {
    return next(error);
  }
};

// UPDATE POSISI
const UpdatePosisi = async (req, res, next) => {
  const { id } = req.params;
  const { posisi } = req.body;
  try {
    const update = await db(tableName.posisi).update({ posisi }).where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetAllPosisi, CreatePosisi, BlokirPosisi, UpdatePosisi };
