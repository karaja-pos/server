const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL SATUAN
const GetAllSatuan = async (req, res, next) => {
  try {
    const data = await db(tableName.satuan).select("*");
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATE SATUAN
const CreateSatuan = async (req, res, next) => {
  const { nama_satuan, user } = req.body;
  try {
    const add = await db(tableName.satuan).insert({ nama_satuan });
    return WebResponse(res, 201, "Created", add);
  } catch (error) {
    return next(error);
  }
};

const UpdateSatuan = async (req, res, next) => {
  const { id } = req.params;
  const { nama_satuan } = req.body;
  try {
    const update = await db(tableName.satuan)
      .update({ nama_satuan })
      .where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

// SET active
const SetActive = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.satuan).update({ status }).where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetAllSatuan, CreateSatuan, SetActive, UpdateSatuan };
