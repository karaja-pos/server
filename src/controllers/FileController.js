const importExcell = require("convert-excel-to-json");
const del = require("del");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const UploadStok = (req, res, next) => {
  const filename = req.file.filename;
  const dataImport = importExcell({
    sourceFile: __dirname + "/../public/uploads/stok/" + filename,
    header: { rows: 1 },
    columnToKey: {
      A: "ID",
      B: "ID_BARANG",
      C: "NAMA_BARANG",
      D: "STOK",
      E: "HARGA",
      F: "ID_TOKO",
    },
    sheets: ["Sheet1"],
  });
  const DATA = dataImport.Sheet1.map((x) => {
    return {
      id: x.ID,
      stok_akhir: x.STOK,
      harga_jual: x.HARGA,
    };
  });

  return db.transaction((trx) => {
    var queries = [];
    DATA.map((x) => {
      const query = db(tableName.stok_toko)
        .where({ id: x.id })
        .update({ harga_jual: x.harga_jual })
        .increment({ stok_akhir: x.stok_akhir })
        .transacting(trx);
      queries.push(query);
    });

    return Promise.all(queries)
      .then(async () => {
        await trx.commit;
        await del([__dirname + "/../public/uploads/stok/*.xlsx"], {
          dryRun: true,
        });
        return WebResponse(res, 201, "Success");
      })
      .catch((e) => {
        trx.rollback;
        return next(e);
      });
  });
};

module.exports = { UploadStok };

// knex.transaction(function(trx) {
//   knex('users')
//       .transactiong(trx)
//       .select('id')
//       .select('firstName')
//       .then(async function(res) {
//           for (let i = 0; i < res.length; i++) {
//               const userData = res[i]; //here we have id and firstName
//               const encryptedFirstName = encrypt(userData.firstName);
//               trx('users')
//                   .where('id', '=', userData.id)
//                   .update({
//                       firstName: encryptedFirstName
//                   })
//           }
//       })
//       .then(function() {
//          trx.commit();
//          process.exit(0);
//       })
//       .catch(function(err) {
//          trx.rollback();
//          process.exit(1);
//       })
// });
