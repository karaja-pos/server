const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL SUPPLIER
const GetAllSuplier = async (req, res, next) => {
  try {
    const data = await db(tableName.suplier).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED SUPPLIER
const CreateSuplier = async (req, res, next) => {
  const {
    kode_supplier,
    nama_supplier,
    telp_supplier,
    alamat_supplier,
    user,
  } = req.body;
  try {
    const addSuplier = await db(tableName.suplier).insert({
      kode_supplier,
      nama_supplier,
      telp_supplier,
      alamat_supplier,
      user,
    });

    if (addSuplier) {
      return WebResponse(res, 201, "Created", addSuplier);
    }
  } catch (error) {
    return next(error);
  }
};

const UpdateSupplier = async (req, res, next) => {
  const { id } = req.params;
  const { nama_supplier, telp_supplier, alamat_supplier } = req.body;
  try {
    const update = await db(tableName.suplier)
      .update({
        nama_supplier,
        telp_supplier,
        alamat_supplier,
      })
      .where({ id });

    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

const BlokirSupplier = async (req, res, next) => {
  const { id } = req.params;
  const { blokir } = req.body;
  try {
    const update = await db(tableName.suplier).update({ blokir }).where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  CreateSuplier,
  GetAllSuplier,
  UpdateSupplier,
  BlokirSupplier,
};
