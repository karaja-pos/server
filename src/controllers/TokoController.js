const bcrypt = require("bcryptjs");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL TOKO
const GetAllToko = async (req, res, next) => {
  try {
    const data = await db(tableName.toko).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED TOKO
const CreateToko = async (req, res, next) => {
  const { kode_toko, nama_toko, alamat, telp, pajak, user } = req.body;

  try {
    const passwordHash = await bcrypt.hashSync(kode_toko.toLowerCase(), 12);

    const addToko = await db(tableName.toko).insert({
      kode_toko,
      nama_toko,
      alamat,
      telp,
      pajak,
      user,
    });

    await db(tableName.users).insert({
      email: nama_toko,
      username: kode_toko.toLowerCase(),
      password: passwordHash,
      role: "toko",
      id_pengguna: addToko[0],
      status: 1,
    });

    if (addToko) {
      return WebResponse(res, 201, "Created", addToko);
    }
  } catch (error) {
    console.log(error);
    return next(error);
  }
};

// UPDATE TOKO BY ID
const UpdateTokoById = async (req, res, next) => {
  const { id } = req.params;
  const { nama_toko, alamat, telp } = req.body;
  try {
    const update = await db(tableName.toko)
      .update({ nama_toko, alamat, telp })
      .where({ id });

    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

// UPDATE STATUS
const UpdateStatusToko = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.toko).update({ status }).where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetAllToko, CreateToko, UpdateTokoById, UpdateStatusToko };
