const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// Get MEREK
const GetAllMerek = async (req, res, next) => {
  try {
    const data = await db(tableName.merek).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED MEREK
const CreateMerek = async (req, res, next) => {
  const { merek, user } = req.body;
  try {
    const addMerek = await db(tableName.merek).insert({
      merek,
      user,
    });

    if (addMerek) {
      return WebResponse(res, 201, "Created", addMerek);
    }
  } catch (error) {
    return next(error);
  }
};

const UpdateMerek = async (req, res, next) => {
  const { id } = req.params;
  const { merek } = req.body;
  try {
    const update = await db(tableName.merek)
      .update({
        merek,
      })
      .where({ id });

    return WebResponse(res, 201, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

const SetStatus = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.merek).update({ status }).where({ id });

    return WebResponse(res, 201, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetAllMerek, CreateMerek, UpdateMerek, SetStatus };
