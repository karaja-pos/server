const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const WebResponse = require("../utils/WebResponse");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const { users } = require("../../db/constant/tableName");

const UserRegister = async (req, res, next) => {
  const { email, username, password, role, status } = req.body;
  try {
    const checkEmail = await db(tableName.users)
      .where({ username })
      .select("email", "username", "password", "role", "status");

    const passwordHash = await bcrypt.hashSync(password, 12);

    if (checkEmail.length > 0) {
      return WebResponse(res, 200, "Success", "Username sudah terdaftar");
    }

    await db(tableName.users)
      .insert({
        email,
        username,
        password: passwordHash,
        role,
        status,
      })
      .then((result) => {
        return WebResponse(res, 200, "Success", "Users Created!");
      });
  } catch (error) {
    return next(error);
  }
};

const GetUsers = async (req, res, next) => {
  try {
    const checkEmail = await db(tableName.users).select(
      "uuid",
      "email",
      "username",
      "role",
      "status"
    );
    // .whereNot({ role: "admin" });
    return WebResponse(res, 200, "Success", checkEmail);
  } catch (error) {
    return next(error);
  }
};

const UserLogin = async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const user = await db(tableName.users).where({ username });
    if (user.length > 0) {
      const match = await bcrypt.compare(password, user[0].password);
      if (match) {
        switch (user[0].role) {
          case "admin":
            const dataAdmin = {
              nama: user[0].email,
              role: user[0].role,
            };
            const tokenAdmin = await jwt.sign(dataAdmin, "7qvt6t2738", {
              expiresIn: 86400,
            });
            return WebResponse(res, 201, "Success", tokenAdmin);
            break;
          case "toko":
            const getToko = await db(tableName.users)
              .where({ uuid: user[0].uuid })
              .select(
                `${tableName.toko}.id`,
                `${tableName.toko}.nama_toko`,
                `${tableName.toko}.kode_toko`,
                `${tableName.users}.role`
              )
              .join(
                tableName.toko,
                `${tableName.users}.id_pengguna`,
                `${tableName.toko}.id`
              );
            const dataToko = {
              nama: getToko[0].nama_toko,
              role: getToko[0].role,
              id_toko: getToko[0].id,
              kode_toko: getToko[0].kode_toko,
            };
            const tokenToko = await jwt.sign(dataToko, "7qvt6t2738", {
              expiresIn: 86400,
            });
            return WebResponse(res, 201, "Success", tokenToko);
            break;

          // case "user":
          //   const getUser = await db(tableName.users)
          //     .where({ uuid: user[0].uuid })
          //     .select(
          //       `${tableName.karyawan}.id`,
          //       `${tableName.karyawan}.nama_karyawan`,
          //       `${tableName.karyawan}.kode_karyawan`,
          //       `${tableName.users}.role`
          //     )
          //     .join(
          //       tableName.karyawan,
          //       `${tableName.users}.id_pengguna`,
          //       `${tableName.karyawan}.id`
          //     );
          //   const dataUser = {
          //     nama: getUser[0].nama_karyawan,
          //     role: getUser[0].role,
          //     id_toko: getUser[0].id,
          //     kode_toko: getUser[0].kode_karyawan,
          //   };
          //   const tokenUser = await jwt.sign(dataUser, "7qvt6t2738", {});
          //   return WebResponse(res, 201, "Success", tokenUser);
          //   break;

          default:
            break;
        }
      } else {
        return WebResponse(res, 201, "error", "Username atau password salah");
      }
    } else {
      return WebResponse(res, 201, "error", "Username atau password salah");
    }
  } catch (error) {}
};

module.exports = {
  UserRegister,
  GetUsers,
  UserLogin,
};
