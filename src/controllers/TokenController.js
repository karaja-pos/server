const bcrypt = require("bcryptjs");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const CheckToken = async (req, res, next) => {
  const { token } = req.params;
  try {
    const data = await db(tableName.token)
      .where({ token })
      .join(
        tableName.toko,
        `${tableName.token}.id_toko`,
        `${tableName.toko}.id`
      );
    if (data.length > 0) {
      return WebResponse(res, 200, "Terdaftar", data[0].kode_toko);
    } else {
      return WebResponse(res, 200, "Error", "ERROR");
    }
  } catch (error) {
    return next(error);
  }
};

const GetAllToken = async (req, res, next) => {
  try {
    const data = await db(tableName.token)
      .select("*")
      .join(
        tableName.toko,
        `${tableName.token}.id_toko`,
        `${tableName.toko}.id`
      );
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = { CheckToken, GetAllToken };
