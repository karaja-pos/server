const consola = require("consola");
const multer = require("multer");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET SEMUA BARANG
const GetSemuaBarang = async (req, res, next) => {
  try {
    const data = await db(tableName.barang)
      .select(
        `${tableName.barang}.id`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.deskripsi`,
        `${tableName.barang}.stok_akhir`,
        `${tableName.kategori}.id as id_kategori`,
        `${tableName.kategori}.kategori`,
        `${tableName.merek}.id as id_merek`,
        `${tableName.merek}.merek`,
        `${tableName.satuan}.id as id_satuan`,
        `${tableName.satuan}.nama_satuan`,
        `${tableName.barang}.gambar_brg`
        // `${tableName.file}.type`,
        // `${tableName.file}.file`
      )
      .join(
        tableName.kategori,
        `${tableName.barang}.id_kategori`,
        `${tableName.kategori}.id`
      )
      .join(
        tableName.merek,
        `${tableName.barang}.id_merek`,
        `${tableName.merek}.id`
      )
      .leftJoin(
        tableName.satuan,
        `${tableName.barang}.id_satuan`,
        `${tableName.satuan}.id`
      );

    // .leftJoin(
    //   tableName.file,
    //   `${tableName.barang}.gambar_brg`,
    //   `${tableName.file}.id`
    // );
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetBarangByToko = async (req, res, next) => {
  const { id } = req.params;
  try {
    const stok = await db(tableName.stok_toko)
      .select("id_barang")
      .where({ id_toko: id });
    // .andWhere("status", "Y");
    const dataArr = [];
    stok.forEach((i) => {
      dataArr.push(i.id_barang);
    });

    const dataBarang = await db(tableName.barang)
      .select("*")
      .whereNotIn("id", dataArr);

    return WebResponse(res, 200, "Success", dataBarang);
  } catch (error) {
    return next(error);
  }
};

const GetStokTokoById = async (req, res, next) => {
  const { id } = req.params;
  try {
    const data = await db(tableName.stok_toko)
      .select(
        `${tableName.stok_toko}.id`,
        `${tableName.barang}.id as id_barang`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.nama_brg as nama_barang`,
        `${tableName.toko}.id as id_toko`,
        `${tableName.toko}.nama_toko`,
        `${tableName.stok_toko}.harga_jual`,
        `${tableName.stok_toko}.stok_akhir`,
        `${tableName.stok_toko}.diskon`,
        `${tableName.kategori}.id as id_kategori`,
        `${tableName.kategori}.kategori`,
        `${tableName.merek}.merek`,
        `${tableName.stok_toko}.status`
      )
      .join(
        tableName.barang,
        `${tableName.stok_toko}.id_barang`,
        `${tableName.barang}.id`
      )
      //test
      .join(
        tableName.toko,
        `${tableName.stok_toko}.id_toko`,
        `${tableName.toko}.id`
      )
      .join(
        tableName.kategori,
        `${tableName.barang}.id_kategori`,
        `${tableName.kategori}.id`
      )
      .join(
        tableName.merek,
        `${tableName.barang}.id_merek`,
        `${tableName.merek}.id`
      )
      .where({ id_toko: id })
      .andWhere(`${tableName.stok_toko}.status`, "Y")
      .orderBy(`${tableName.stok_toko}.id_barang`);
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATE BARANG
const CreateBarang = async (req, res, next) => {
  const {
    id_kategori,
    kode_brg,
    id_merek,
    nama_brg,
    deskripsi,
    stok_akhir,
    id_satuan,
    user,
  } = req.body;
  // const { file } = req.files;

  try {
    const tambahBarang = await db(tableName.barang).insert({
      id_kategori,
      kode_brg,
      id_merek,
      nama_brg,
      deskripsi,
      stok_akhir,
      id_satuan,
      user,
    });
    if (tambahBarang) {
      return WebResponse(res, 201, "Created", tambahBarang);
    }
  } catch (error) {
    return next(error);
  }
};

// UPDATE BARANG
const UpdateBarang = async (req, res, next) => {
  const { id } = req.params;
  const {
    id_kategori,
    kode_brg,
    id_merek,
    nama_brg,
    deskripsi,
    stok_akhir,
    id_satuan,
  } = req.body;
  try {
    if (req.file === undefined) {
      const update = await db(tableName.barang)
        .update({
          id_kategori,
          kode_brg,
          id_merek,
          nama_brg,
          deskripsi,
          stok_akhir,
          id_satuan,
        })
        .where({ id });
      return WebResponse(res, 201, "Updated No IMG", update);
    } else {
      const update = await db(tableName.barang)
        .update({
          id_kategori,
          kode_brg,
          id_merek,
          nama_brg,
          deskripsi,
          stok_akhir,
          gambar_brg: req.file.filename,
          id_satuan,
        })
        .where({ id });
      return WebResponse(res, 201, "Updated", update);
    }
    // if (req.files !== null) {
    //   const simpanFile = await db(tableName.file).insert({
    //     title: nama_brg,
    //     type: req.files.file.mimetype,
    //     file: req.files.file.data,
    //     user: "",
    //   });
    //   const update = await db(tableName.barang)
    //     .update({
    //       id_kategori,
    //       kode_brg,
    //       id_merek,
    //       nama_brg,
    //       deskripsi,
    //       stok_akhir,
    //       gambar_brg: simpanFile[0],
    //       id_satuan,
    //     })
    //     .where({ id });
    //   return WebResponse(res, 201, "Updated", update);
    // } else {

    // }
  } catch (error) {
    return next(error);
  }
};

const GetBarangByCode = async (req, res, next) => {
  const { code } = req.params;
  try {
    const data = await db(tableName.barang)
      .select("*")
      .where({ kode_brg: code });

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const InsertDataInStokToko = async (req, res, next) => {
  const { data } = req.body;
  try {
    const add = await db(tableName.stok_toko).insert(data);
    return WebResponse(res, 201, "Created", add);
  } catch (error) {
    return next(error);
  }
};

const SetTotalHarga = async (req, res, next) => {
  const { id } = req.params;
  const { harga } = req.body;
  console.log(req.body);
  try {
    const setData = await db(tableName.stok_toko)
      .update({ harga_jual: harga })
      .where({ id });
    return WebResponse(res, 200, "Updated", setData);
  } catch (error) {
    return next(error);
  }
};

const SetStokBarang = async (req, res, next) => {
  const { id } = req.params;
  const { stok_akhir } = req.body;
  try {
    const setData = await db(tableName.stok_toko)
      .update({ stok_akhir })
      .where({ id });
    return WebResponse(res, 200, "Updated", setData);
  } catch (error) {
    return next(error);
  }
};

const SetDiskonBarang = async (req, res, next) => {
  const { id } = req.params;
  const { diskon } = req.body;
  try {
    const setData = await db(tableName.stok_toko)
      .update({ diskon })
      .where({ id });
    return WebResponse(res, 200, "Updated", setData);
  } catch (error) {
    return next(error);
  }
};

const UpdateStatusStok = async (req, res, next) => {
  const { id, status } = req.query;
  try {
    const data = await db(tableName.stok_toko)
      .where({ id })
      .update({ status }, ["id", "status"]);
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetSemuaBarang,
  CreateBarang,
  GetBarangByCode,
  UpdateBarang,
  GetStokTokoById,
  GetBarangByToko,
  InsertDataInStokToko,
  SetTotalHarga,
  SetStokBarang,
  SetDiskonBarang,
  UpdateStatusStok,
};
