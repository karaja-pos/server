const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL KATEGORI
const GetAllKategori = async (req, res, next) => {
  try {
    const data = await db(tableName.kategori).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATE KATEGORI
const CreateKategori = async (req, res, next) => {
  const { kategori, user } = req.body;
  try {
    const addData = await db(tableName.kategori).insert({
      kategori,
      user,
    });
    // req.io.sockets.emit("create-kategori", "create-kategori");
    if (addData) {
      return WebResponse(res, 201, "Created", addData);
    }
  } catch (error) {
    return next(error);
  }
};

// UPDATE KATEGORI
const UpdateKategori = async (req, res, next) => {
  const { id } = req.params;
  const { kategori } = req.body;
  try {
    const update = await db(tableName.kategori)
      .update({
        kategori,
      })
      .where({ id });

    return WebResponse(res, 201, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

const SetStatus = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.kategori)
      .update({ status })
      .where({ id });

    return WebResponse(res, 201, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetAllKategori, CreateKategori, SetStatus, UpdateKategori };
