const moment = require("moment");
const db = require("../../db");
const { toko } = require("../../db/constant/tableName");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const CreatePenarikan = async (req, res, next) => {
  const { toko_tujuan, toko_asal, id_barang, jumlah_item, user } = req.body;
  console.log(req.body);
  try {
    const add = await db(tableName.penarikan).insert({
      toko_tujuan,
      toko_asal,
      id_barang,
      jumlah_item,
      tgl_penarikan: moment().format("yyyy-MM-DD"),
      user,
    });

    const data = {
      toko_tujuan,
      toko_asal,
      id_barang,
    };
    req.io.sockets.emit("penarikan-barang", data);
    req.io.sockets.emit("penarikan-tujuan", toko_tujuan);
    // req.io.sockets.emit("penarikan-asal", toko_asal);
    return WebResponse(res, 201, "Created");
  } catch (error) {
    return next(error);
  }
};

const GetAllPenarikan = async (req, res, next) => {
  try {
    const data = await db(tableName.penarikan)
      .select(
        `${tableName.penarikan}.*`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.nama_brg`
        // `${tableName.toko}.nama_toko`
      )
      .join(
        tableName.barang,
        `${tableName.penarikan}.id_barang`,
        `${tableName.barang}.id`
      )
      .orderBy("id", "desc");
    const dataToko = await db(tableName.toko).select("id", "nama_toko");
    const dataArr = data.map((x) => {
      const xAsal = dataToko.find((i) => i.id === x.toko_asal);
      const xTujuan = dataToko.find((i) => i.id === x.toko_tujuan);
      return {
        id: x.id,
        tgl_penarikan: moment(x.tgl_penarikan).format(),
        jumlah_item: x.jumlah_item,
        id_barang: x.id_barang,
        baca: x.baca,
        kode_brg: x.kode_brg,
        nama_brg: x.nama_brg,
        status: x.status,
        id_asal: x.toko_asal,
        toko_asal: xAsal,
        id_tujuan: x.toko_tujuan,
        toko_tujuan: xTujuan,
      };
    });

    return WebResponse(res, 200, "Success", dataArr);
  } catch (error) {
    return next(error);
  }
};

const GetPenarikanByToko = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.penarikan)
      .select(
        `${tableName.penarikan}.id`,
        `${tableName.penarikan}.toko_asal`,
        `${tableName.penarikan}.toko_tujuan`,
        `${tableName.penarikan}.tgl_penarikan`,
        `${tableName.penarikan}.jumlah_item`,
        `${tableName.penarikan}.id_barang`,
        `${tableName.penarikan}.baca`,
        `${tableName.penarikan}.status`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.nama_brg`
        // `${tableName.toko}.nama_toko`
      )
      .where({
        toko_tujuan: id_toko,
      })
      .orWhere({ toko_asal: id_toko })
      .join(
        tableName.barang,
        `${tableName.penarikan}.id_barang`,
        `${tableName.barang}.id`
      )
      .orderBy("id", "desc");

    const dataToko = await db(tableName.toko).select("id", "nama_toko");

    const dataArr = data.map((x) => {
      const xAsal = dataToko.find((i) => i.id === x.toko_asal);
      const xTujuan = dataToko.find((i) => i.id === x.toko_tujuan);
      return {
        id: x.id,
        tgl_penarikan: moment(x.tgl_penarikan).format(),
        jumlah_item: x.jumlah_item,
        id_barang: x.id_barang,
        baca: x.baca,
        kode_brg: x.kode_brg,
        nama_brg: x.nama_brg,
        status: x.status,
        id_asal: x.toko_asal,
        toko_asal: xAsal,
        id_tujuan: x.toko_tujuan,
        toko_tujuan: xTujuan,
      };
    });

    return WebResponse(res, 200, "Success", dataArr);
  } catch (error) {
    return next(error);
  }
};

const GetCountPenarikan = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.penarikan)
      .select("*")
      .where({
        toko_tujuan: id_toko,
        baca: 0,
      })
      .orWhere({ toko_asal: id_toko });
    return WebResponse(res, 200, "Success", data.length);
  } catch (error) {
    return next(error);
  }
};

const Baca = async (req, res, next) => {
  const { id } = req.params;
  try {
    await db(tableName.penarikan).update({ baca: 1 }).where({ id });
    req.io.sockets.emit("penarikan-baca", "baca");
    return WebResponse(res, 201, "Updated");
  } catch (error) {
    return next(error);
  }
};

const GetToko = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.toko).whereNot("id", id_toko);
    return WebResponse(res, 200, "success", data);
  } catch (error) {
    return next(error);
  }
};

const HandleSetuju = async (req, res, next) => {
  const { id } = req.params;
  // const {id, jumlah } = req.query
  try {
    const data = await db(tableName.penarikan).where({ id });
    const tokoAsal = await db(tableName.stok_toko).where({
      id_toko: data[0].toko_asal,
      id_barang: data[0].id_barang,
    });

    const tokoTujuan = await db(tableName.stok_toko).where({
      id_toko: data[0].toko_tujuan,
      id_barang: data[0].id_barang,
    });

    if (tokoTujuan.length <= 0) {
      return WebResponse(
        res,
        200,
        "Error",
        "Jenis barang ini belum di mapping di toko tujuan"
      );
    }

    // const stokTokoAsal = tokoAsal[0].stok_akhir - data[0].jumlah_item;
    // const stokTokoTujuan = tokoTujuan[0].stok_akhir + data[0].jumlah_item;

    // console.log(stokTokoTujuan);
    // console.log(stokTokoAsal);

    await db(tableName.stok_toko)
      .decrement({ stok_akhir: data[0].jumlah_item })
      .where({ id_barang: data[0].id_barang })
      .where({ id_toko: data[0].toko_asal });

    await db(tableName.stok_toko)
      .increment({ stok_akhir: data[0].jumlah_item })
      .where({ id_barang: data[0].id_barang })
      .where({ id_toko: data[0].toko_tujuan });

    const update = await db(tableName.penarikan)
      .update({ status: 2 })
      .where({ id });

    if (update) {
      req.io.sockets.emit("penarikan-tujuan", data[0].toko_tujuan);
      req.io.sockets.emit("penarikan-asal", data[0].toko_asal);
      return WebResponse(res, 200, "Updated", update);
    }
  } catch (error) {
    return next(error);
  }
};

const HandleTolak = async (req, res, next) => {
  const { id } = req.params;
  try {
    const data = await db(tableName.penarikan).where({ id });
    const update = await db(tableName.penarikan)
      .update({ status: 3 })
      .where({ id });
    if (update) {
      req.io.sockets.emit("penarikan-tujuan", data[0].toko_tujuan);
      req.io.sockets.emit("penarikan-asal", data[0].toko_asal);
      return WebResponse(res, 200, "Updated", update);
    }
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  CreatePenarikan,
  GetPenarikanByToko,
  GetCountPenarikan,
  Baca,
  GetToko,
  GetAllPenarikan,
  HandleTolak,
  HandleSetuju,
};
