const consola = require("consola");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const HistoryTransaksi = async (req, res, next) => {
  const { id_toko, tgl_start, tgl_end, jenis } = req.query;
  try {
    switch (jenis) {
      case "TUNAI": {
        const dataTunai = await db(tableName.penjualanTunai)
          .select(
            `${tableName.penjualanTunai}.id`,
            `${tableName.penjualanTunai}.no_penjualan`,
            `${tableName.penjualanTunai}.tgl_penjualan`,
            `${tableName.penjualanTunai}.jam_penjualan`,
            `${tableName.penjualanTunai}.total_item`,
            `${tableName.penjualanTunai}.jenis_bayar`,
            `${tableName.penjualanTunai}.id_toko`,
            `${tableName.penjualanTunai}.total_harga`,
            `${tableName.penjualanTunai}.created_at`,
            `${tableName.penjualanTunai}.updated_at`
          )
          .join(
            tableName.toko,
            `${tableName.penjualanTunai}.id_toko`,
            `${tableName.toko}.id`
          )
          .where({ id_toko, jenis_bayar: 1 })
          .andWhereBetween("tgl_penjualan", [tgl_start, tgl_end])
          .orderBy(`${tableName.penjualanTunai}.id`, "desc");

        return WebResponse(res, 200, "Success", dataTunai);
      }

      case "NONTUNAI": {
        const dataNonTunai = await db(tableName.penjualanTunai)
          .where({ id_toko, jenis_bayar: 2 })
          .andWhereBetween("tgl_penjualan", [tgl_start, tgl_end]);
        return WebResponse(res, 200, "Success", dataNonTunai);
      }

      case "KREDIT": {
      }

      default:
        break;
    }
    return WebResponse(res, 200, "Success");
  } catch (error) {
    return next(error);
  }
};

const HistoryTransaksiByNo = async (req, res, next) => {
  const { jenis, no_penjualan } = req.params;
  try {
    switch (jenis) {
      case "TUNAI": {
        const dataTunai = await db(tableName.tampungPenjualanTunai)
          .where({
            no_penjualan,
          })
          .join(
            tableName.barang,
            `${tableName.tampungPenjualanTunai}.id_barang`,
            `${tableName.barang}.id`
          );
        return WebResponse(res, 200, "Success", dataTunai);
      }

      case "NONTUNAI": {
        const dataNonTunai = await db(tableName.penjualanTunai)
          .where({
            no_penjualan,
            jenis_bayar: 2,
          })
          .join(
            tableName.barang,
            `${tableName.tampungPenjualanTunai}.id_barang`,
            `${tableName.barang}.id`
          );
        return WebResponse(res, 200, "Success", dataNonTunai);
      }

      case "KREDIT": {
      }

      default:
        break;
    }
  } catch (error) {
    return next(error);
  }
};

const GetTransaksiByNoPenjualan = async (req, res, next) => {
  const { no_penjualan } = req.params;
  try {
    const data = await db(tableName.tampungPenjualanTunai)
      .select(
        `${tableName.barang}.id as id_barang`,
        `${tableName.barang}.nama_brg`,
        `${tableName.tampungPenjualanTunai}.no_penjualan`,
        `${tableName.tampungPenjualanTunai}.jumlah_item`,
        `${tableName.tampungPenjualanTunai}.diskon`,
        `${tableName.tampungPenjualanTunai}.harga_diskon`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.barang}.id`
      )
      .where({ no_penjualan });

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const BatalkanTransaksi = async (req, res, next) => {
  const { jenis, no_penjualan } = req.params;
  try {
    switch (jenis) {
      case "TUANI": {
      }

      case "NON_TUANI": {
      }

      default:
        break;
    }
    await db(tableName.penjualanTunai);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  HistoryTransaksi,
  HistoryTransaksiByNo,
  GetTransaksiByNoPenjualan,
};
