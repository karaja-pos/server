const moment = require("moment");
const jwt = require("jsonwebtoken");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const CheckKasir = async (req, res, next) => {
  try {
    const data = await db(tableName.karyawan);
  } catch (error) {
    return next(error);
  }
};

const KasirLogin = async (req, res, next) => {
  const { kode_karyawan, kode_toko } = req.body;
  console.log(kode_toko);
  try {
    const checkKasir = await db(tableName.karyawan)
      .where({
        kode_karyawan,
      })
      .select(
        `${tableName.karyawan}.id`,
        `${tableName.karyawan}.kode_karyawan`,
        `${tableName.karyawan}.nama_karyawan`,
        `${tableName.karyawan}.nomor_hp`,
        `${tableName.karyawan}.blokir`,
        `${tableName.toko}.id as id_toko`,
        `${tableName.toko}.kode_toko`,
        `${tableName.toko}.nama_toko`,
        `${tableName.toko}.alamat`,
        `${tableName.toko}.telp`,
        `${tableName.toko}.pajak`
      )
      .join(
        tableName.toko,
        `${tableName.karyawan}.id_toko`,
        `${tableName.toko}.id`
      );

    if (checkKasir.length > 0) {
      if (checkKasir[0].kode_toko === kode_toko || kode_toko === "M-KIOS") {
        const data = {
          ...checkKasir[0],
        };

        const token = await jwt.sign(data, "7qvt6t2738", {expiresIn: 86400})
        req.io.sockets.emit("kasir-login", kode_karyawan);
        await db(tableName.karyawan)
          .update({ status: "Y" })
          .where({ kode_karyawan });
        return WebResponse(res, 200, "Login", token);
      } else {
        return WebResponse(
          res,
          300,
          "Anda bukan karyawan di toko ini atau andah sudah di blokir ,silahkan hubungi admin"
        );
      }
    } else {
      return WebResponse(
        res,
        300,
        "Anda bukan karyawan di toko ini atau andah sudah di blokir ,silahkan hubungi admin"
      );
    }
  } catch (error) {
    return next(error);
  }
};

const KasirLogout = async (req, res, next) => {
  const { kode_karyawan } = req.body;
  try {
    req.io.sockets.emit("kasir-login", kode_karyawan);
    await db(tableName.karyawan)
      .update({ status: "N" })
      .where({ kode_karyawan });
    return WebResponse(res, 200, "Logout", kode_karyawan);
  } catch (error) {
    return next(error);
  }
};

const GetBarangToko = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.stok_toko)
      .where({ id_toko })
      .select(
        `${tableName.stok_toko}.id`,
        `${tableName.stok_toko}.id_barang`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.deskripsi`,
        `${tableName.barang}.id_kategori`,
        `${tableName.barang}.id_satuan`,
        `${tableName.satuan}.nama_satuan`,
        `${tableName.kategori}.kategori`,
        `${tableName.stok_toko}.harga_jual`,
        `${tableName.stok_toko}.stok_akhir`,
        `${tableName.stok_toko}.diskon`
        // `${tableName.file}.type`,
        // `${tableName.file}.file`
      )
      .join(
        tableName.barang,
        `${tableName.stok_toko}.id_barang`,
        `${tableName.barang}.id`
      )
      .join(
        tableName.satuan,
        `${tableName.barang}.id_satuan`,
        `${tableName.satuan}.id`
      )
      .join(
        tableName.kategori,
        `${tableName.barang}.id_kategori`,
        `${tableName.kategori}.id`
      )
      .whereNot({ harga_jual: 0 });
    // .leftJoin(
    //   tableName.file,
    //   `${tableName.barang}.gambar_brg`,
    //   `${tableName.file}.id`
    // );

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const TransaksiKasir = async (req, res, next) => {
  const {
    id_kasir,
    id_toko,
    jam_penjualan,
    no_penjualan,
    pajak,
    tgl_penjualan,
    total_diskon,
    sub_total,
    total_harga,
    total_item,
    barang,
    penjualan,
    id_kreditur,
    nama_pembeli,
    nik,
    dp,
    rek_tujuan,
    rek_pengirim,
    tgl_transfer,
    jam_transfer,
  } = req.body;
  try {
    const checkPenjualan = await db(tableName.penjualanTunai).where({
      no_penjualan,
    });
    if (checkPenjualan.length > 0) {
      return WebResponse(res, 201, "Error");
    } else {
      await barang.map((item) => {
        db(tableName.stok_toko)
          .where({ id_toko, id_barang: item.id_barang })
          .then((result) => {
            db(tableName.stok_toko)
              .where({ id_toko, id_barang: item.id_barang })
              .decrement({ stok_akhir: item.quantity })
              // .update({ stok_akhir: result[0].stok_akhir - })
              .then(() => {
                return {
                  no_penjualan: no_penjualan,
                  id_barang: item.id_barang,
                  id_satuan: item.id_satuan,
                  jumlah_item: item.quantity,
                  harga_satuan: item.harga_jual,
                  diskon: item.diskon,
                  harga_diskon: item.hargaDiskon,
                };
              });
          });
      });

      const barangArr = await barang.map((item) => {
        console.log(item);
        if (item.jenis === "JUAL") {
          return {
            no_penjualan: no_penjualan,
            id_barang: item.id_barang,
            id_satuan: item.id_satuan,
            jumlah_item: item.quantity,
            harga_satuan: item.harga_jual,
            diskon: item.diskon,
            harga_diskon: item.hargaDiskon,
          };
        }
      });

      console.log(barangArr);

      await barang.map(async (item) => {
        if (item.jenis === "BONUS") {
          await db(tableName.tampungBonus).insert({
            no_penjualan: no_penjualan,
            id_barang: item.id_barang,
            id_satuan: item.id_satuan,
            jumlah_item: item.quantity,
            harga_satuan: item.harga_jual,
            diskon: item.diskon,
            harga_diskon: item.hargaDiskon,
          });
        }
      });

      switch (penjualan) {
        case "TUNAI": {
          await db(tableName.penjualanTunai).insert({
            no_penjualan,
            tgl_penjualan: moment(tgl_penjualan).format("yyyy-MM-DD"),
            jam_penjualan,
            total_item,
            sub_total,
            total_harga,
            total_diskon,
            pajak,
            id_toko,
            session_kasir: id_kasir,
          });
          await db(tableName.tampungPenjualanTunai).insert(barangArr);
          req.io.sockets.emit("transaksi-kasir", "TUNAI");
          return WebResponse(res, 201, "Success");
        }

        case "NONTUNAI": {
          await db(tableName.penjualanTunai).insert({
            no_penjualan,
            tgl_penjualan: moment(tgl_penjualan).format("yyyy-MM-DD"),
            jam_penjualan,
            total_item,
            sub_total,
            total_harga,
            total_diskon,
            pajak,
            id_toko,
            rek_tujuan,
            rek_pengirim,
            tgl_transfer: moment(tgl_transfer).format("yyyy-MM-DD"),
            jam_transfer,
            nama_pengirim: nama_pembeli,
            jenis_bayar: 2,
            session_kasir: id_kasir,
          });
          await db(tableName.tampungPenjualanTunai).insert(barangArr);
          req.io.sockets.emit("transaksi-kasir", "TUNAI");
          return WebResponse(res, 201, "Success");
        }

        case "KREDIT": {
          await db(tableName.penjualanKredit).insert({
            no_penjualan,
            tgl_penjualan: moment(tgl_penjualan).format("yyyy-MM-DD"),
            jam_penjualan,
            total_item,
            sub_total,
            total_harga,
            total_diskon,
            pajak,
            id_toko,
            id_kreditur,
            nama_pembeli,
            nik,
            dp,
            session_kasir: id_kasir,
          });
          await db(tableName.tampungPenjualanKredit).insert(barangArr);
          req.io.sockets.emit("transaksi-kasir", "KREDIT");
          return WebResponse(res, 201, "Success");
        }

        default:
          break;
      }
    }
  } catch (error) {
    return next(error);
  }
};

const HistoryPenjualanByToko = async (req, res, next) => {
  // const { id_toko, tgl_start, tgl_end, jenis } = req.query;
  try {
    // const penjualan = await db(tableName)

    return WebResponse(res, next, "Success");
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetBarangToko,
  TransaksiKasir,
  KasirLogin,
  KasirLogout,
  HistoryPenjualanByToko,
};
