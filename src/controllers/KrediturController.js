const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// GET ALL TOKO
const GetAllKreditur = async (req, res, next) => {
  try {
    const data = await db(tableName.kreditur).select("*");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

// CREATED TOKO
const CreateKreditur = async (req, res, next) => {
  const { kode, nama_kreditur, alamat, telp, user } = req.body;
  try {
    const addKreditur = await db(tableName.kreditur).insert({
      kode,
      nama_kreditur,
      alamat,
      telp,
      user,
    });

    if (addKreditur) {
      return WebResponse(res, 201, "Created", addKreditur);
    }
  } catch (error) {
    return next(error);
  }
};

// UPDATE TOKO BY ID
const UpdateKrediturById = async (req, res, next) => {
  const { id } = req.params;
  const { nama_kreditur, alamat, telp } = req.body;
  try {
    const update = await db(tableName.kreditur)
      .update({ nama_kreditur, alamat, telp })
      .where({ id });

    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

// UPDATE STATUS
const UpdateStatusKreditur = async (req, res, next) => {
  const { id } = req.params;
  const { status } = req.body;
  try {
    const update = await db(tableName.kreditur)
      .update({ status })
      .where({ id });
    return WebResponse(res, 200, "Updated", update);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetAllKreditur,
  CreateKreditur,
  UpdateStatusKreditur,
  UpdateKrediturById,
};
