const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const KasirAktif = async (req, res, next) => {
  try {
    const data = await db(tableName.karyawan)
      .where({ status: "Y" })
      .count("* as kasir");
    return WebResponse(res, 200, "Success", data[0].kasir);
  } catch (error) {
    return next(error);
  }
};

module.exports = { KasirAktif };
