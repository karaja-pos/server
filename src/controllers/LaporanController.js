const db = require("../../db");
const { penjualanTunai } = require("../../db/constant/tableName");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetLaporanPenjualanTunai = async (req, res, next) => {
  const { toko, kategori, tgl_start, tgl_end, jenis } = req.query;
  try {
    const data = await db(tableName.tampungPenjualanTunai)
      .select(
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.tampungPenjualanTunai}.id_satuan`,
        `${tableName.tampungPenjualanTunai}.harga_satuan`,
        `${tableName.tampungPenjualanTunai}.diskon`,
        `${tableName.tampungPenjualanTunai}.harga_diskon`,
        `${tableName.penjualanTunai}.tgl_penjualan`,
        `${tableName.penjualanTunai}.id_toko`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.id_kategori`
      )
      .join(
        tableName.penjualanTunai,
        `${tableName.tampungPenjualanTunai}.no_penjualan`,
        `${penjualanTunai}.no_penjualan`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.barang}.id`
      )
      .where({ id_toko: toko })
      .where({ id_kategori: kategori })
      .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
        tgl_start,
        tgl_end,
      ]);

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetLaporanTunai = async (req, res, next) => {
  const { toko, kategori, tgl_start, tgl_end } = req.query;

  try {
    if (toko === "undefined" && kategori === "undefined") {
      const dataAll = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 1)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataAll);
    } else if (toko === "undefined") {
      const dataTokoALl = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where({ id_kategori: kategori })
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 1)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataTokoALl);
    } else if (kategori === "undefined") {
      const dataKategoriALl = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where({ id_toko: toko })
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 1)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataKategoriALl);
    }

    const data = await db(tableName.tampungPenjualanTunai)
      .select(
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.tampungPenjualanTunai}.id_satuan`,
        `${tableName.tampungPenjualanTunai}.harga_satuan`,
        `${tableName.tampungPenjualanTunai}.diskon`,
        `${tableName.tampungPenjualanTunai}.harga_diskon`,
        `${tableName.tampungPenjualanTunai}.jumlah_item`,
        `${tableName.penjualanTunai}.tgl_penjualan`,
        `${tableName.penjualanTunai}.id_toko`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.id_kategori`,
        `${tableName.satuan}.nama_satuan`
      )
      .join(
        tableName.penjualanTunai,
        `${tableName.tampungPenjualanTunai}.no_penjualan`,
        `${penjualanTunai}.no_penjualan`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.barang}.id`
      )
      .join(
        tableName.satuan,
        `${tableName.tampungPenjualanTunai}.id_satuan`,
        `${tableName.satuan}.id`
      )
      .where({ id_toko: toko })
      .where({ id_kategori: kategori })
      .where(`${tableName.penjualanTunai}.jenis_bayar`, 1)
      .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
        tgl_start,
        tgl_end,
      ]);

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetLaporanNonTunai = async (req, res, next) => {
  const { toko, kategori, tgl_start, tgl_end } = req.query;
  try {
    if (toko === "undefined" && kategori === "undefined") {
      const dataAll = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 2)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataAll);
    } else if (toko === "undefined") {
      const dataTokoALl = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where({ id_kategori: kategori })
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 2)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataTokoALl);
    } else if (kategori === "undefined") {
      const dataKategoriALl = await db(tableName.tampungPenjualanTunai)
        .select(
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.tampungPenjualanTunai}.harga_satuan`,
          `${tableName.tampungPenjualanTunai}.diskon`,
          `${tableName.tampungPenjualanTunai}.harga_diskon`,
          `${tableName.tampungPenjualanTunai}.jumlah_item`,
          `${tableName.penjualanTunai}.tgl_penjualan`,
          `${tableName.penjualanTunai}.id_toko`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`
        )
        .join(
          tableName.penjualanTunai,
          `${tableName.tampungPenjualanTunai}.no_penjualan`,
          `${penjualanTunai}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanTunai}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanTunai}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .where({ id_toko: toko })
        .where(`${tableName.penjualanTunai}.jenis_bayar`, 2)
        .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataKategoriALl);
    }
    const data = await db(tableName.tampungPenjualanTunai)
      .select(
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.tampungPenjualanTunai}.id_satuan`,
        `${tableName.tampungPenjualanTunai}.harga_satuan`,
        `${tableName.tampungPenjualanTunai}.diskon`,
        `${tableName.tampungPenjualanTunai}.harga_diskon`,
        `${tableName.tampungPenjualanTunai}.jumlah_item`,
        `${tableName.penjualanTunai}.tgl_penjualan`,
        `${tableName.penjualanTunai}.id_toko`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.id_kategori`,
        `${tableName.satuan}.nama_satuan`
      )
      .join(
        tableName.penjualanTunai,
        `${tableName.tampungPenjualanTunai}.no_penjualan`,
        `${penjualanTunai}.no_penjualan`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPenjualanTunai}.id_barang`,
        `${tableName.barang}.id`
      )
      .join(
        tableName.satuan,
        `${tableName.tampungPenjualanTunai}.id_satuan`,
        `${tableName.satuan}.id`
      )
      .where({ id_toko: toko })
      .where({ id_kategori: kategori })
      .where(`${tableName.penjualanTunai}.jenis_bayar`, 2)
      .whereBetween(`${tableName.penjualanTunai}.tgl_penjualan`, [
        tgl_start,
        tgl_end,
      ]);

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetLaporanKredit = async (req, res, next) => {
  const { toko, kategori, tgl_start, tgl_end } = req.query;
  try {
    if (toko === "undefined" && kategori === "undefined") {
      const dataAll = await db(tableName.tampungPenjualanKredit)
        .select(
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.tampungPenjualanKredit}.harga_satuan`,
          `${tableName.tampungPenjualanKredit}.diskon`,
          `${tableName.tampungPenjualanKredit}.harga_diskon`,
          `${tableName.tampungPenjualanKredit}.jumlah_item`,
          `${tableName.penjualanKredit}.tgl_penjualan`,
          `${tableName.penjualanKredit}.id_toko`,
          `${tableName.penjualanKredit}.nama_pembeli`,
          `${tableName.penjualanKredit}.dp`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`,
          `${tableName.kreditur}.nama_kreditur`,
          `${tableName.karyawan}.nama_karyawan`
        )
        .join(
          tableName.penjualanKredit,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.penjualanKredit}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .join(
          tableName.kreditur,
          `${tableName.penjualanKredit}.id_kreditur`,
          `${tableName.kreditur}.id`
        )
        .join(
          tableName.karyawan,
          `${tableName.penjualanKredit}.session_kasir`,
          `${tableName.karyawan}.id`
        )
        // .where({ id_toko: toko })
        // .where({ id_kategori: kategori })
        .whereBetween(`${tableName.penjualanKredit}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataAll);
    } else if (toko === "undefined") {
      const dataTokAll = await db(tableName.tampungPenjualanKredit)
        .select(
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.tampungPenjualanKredit}.harga_satuan`,
          `${tableName.tampungPenjualanKredit}.diskon`,
          `${tableName.tampungPenjualanKredit}.harga_diskon`,
          `${tableName.tampungPenjualanKredit}.jumlah_item`,
          `${tableName.penjualanKredit}.tgl_penjualan`,
          `${tableName.penjualanKredit}.id_toko`,
          `${tableName.penjualanKredit}.nama_pembeli`,
          `${tableName.penjualanKredit}.dp`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`,
          `${tableName.kreditur}.nama_kreditur`,
          `${tableName.karyawan}.nama_karyawan`
        )
        .join(
          tableName.penjualanKredit,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.penjualanKredit}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .join(
          tableName.kreditur,
          `${tableName.penjualanKredit}.id_kreditur`,
          `${tableName.kreditur}.id`
        )
        .join(
          tableName.karyawan,
          `${tableName.penjualanKredit}.session_kasir`,
          `${tableName.karyawan}.id`
        )
        .where({ id_kategori: kategori })
        .whereBetween(`${tableName.penjualanKredit}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataTokAll);
    } else if (kategori === "undefined") {
      const dataKatAll = await db(tableName.tampungPenjualanKredit)
        .select(
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.tampungPenjualanKredit}.harga_satuan`,
          `${tableName.tampungPenjualanKredit}.diskon`,
          `${tableName.tampungPenjualanKredit}.harga_diskon`,
          `${tableName.tampungPenjualanKredit}.jumlah_item`,
          `${tableName.penjualanKredit}.tgl_penjualan`,
          `${tableName.penjualanKredit}.id_toko`,
          `${tableName.penjualanKredit}.nama_pembeli`,
          `${tableName.penjualanKredit}.dp`,
          `${tableName.barang}.nama_brg`,
          `${tableName.barang}.kode_brg`,
          `${tableName.barang}.id_kategori`,
          `${tableName.satuan}.nama_satuan`,
          `${tableName.kreditur}.nama_kreditur`,
          `${tableName.karyawan}.nama_karyawan`
        )
        .join(
          tableName.penjualanKredit,
          `${tableName.tampungPenjualanKredit}.no_penjualan`,
          `${tableName.penjualanKredit}.no_penjualan`
        )
        .join(
          tableName.barang,
          `${tableName.tampungPenjualanKredit}.id_barang`,
          `${tableName.barang}.id`
        )
        .join(
          tableName.satuan,
          `${tableName.tampungPenjualanKredit}.id_satuan`,
          `${tableName.satuan}.id`
        )
        .join(
          tableName.kreditur,
          `${tableName.penjualanKredit}.id_kreditur`,
          `${tableName.kreditur}.id`
        )
        .join(
          tableName.karyawan,
          `${tableName.penjualanKredit}.session_kasir`,
          `${tableName.karyawan}.id`
        )
        .where(`${tableName.penjualanKredit}.id_toko`, toko)
        .whereBetween(`${tableName.penjualanKredit}.tgl_penjualan`, [
          tgl_start,
          tgl_end,
        ]);

      return WebResponse(res, 200, "Success", dataKatAll);
    }

    const data = await db(tableName.tampungPenjualanKredit)
      .select(
        `${tableName.tampungPenjualanKredit}.id_barang`,
        `${tableName.tampungPenjualanKredit}.no_penjualan`,
        `${tableName.tampungPenjualanKredit}.id_satuan`,
        `${tableName.tampungPenjualanKredit}.harga_satuan`,
        `${tableName.tampungPenjualanKredit}.diskon`,
        `${tableName.tampungPenjualanKredit}.harga_diskon`,
        `${tableName.tampungPenjualanKredit}.jumlah_item`,
        `${tableName.penjualanKredit}.tgl_penjualan`,
        `${tableName.penjualanKredit}.id_toko`,
        `${tableName.penjualanKredit}.nama_pembeli`,
        `${tableName.penjualanKredit}.dp`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.kode_brg`,
        `${tableName.barang}.id_kategori`,
        `${tableName.satuan}.nama_satuan`,
        `${tableName.kreditur}.nama_kreditur`,
        `${tableName.karyawan}.nama_karyawan`
      )
      .join(
        tableName.penjualanKredit,
        `${tableName.tampungPenjualanKredit}.no_penjualan`,
        `${tableName.penjualanKredit}.no_penjualan`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPenjualanKredit}.id_barang`,
        `${tableName.barang}.id`
      )
      .join(
        tableName.satuan,
        `${tableName.tampungPenjualanKredit}.id_satuan`,
        `${tableName.satuan}.id`
      )
      .join(
        tableName.kreditur,
        `${tableName.penjualanKredit}.id_kreditur`,
        `${tableName.kreditur}.id`
      )
      .join(
        tableName.karyawan,
        `${tableName.penjualanKredit}.session_kasir`,
        `${tableName.karyawan}.id`
      )
      .where(`${tableName.penjualanKredit}.id_toko`, toko)
      .where({ id_kategori: kategori })
      .whereBetween(`${tableName.penjualanKredit}.tgl_penjualan`, [
        tgl_start,
        tgl_end,
      ]);

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetLaporanPenjualanTunai,
  GetLaporanTunai,
  GetLaporanNonTunai,
  GetLaporanKredit,
};
