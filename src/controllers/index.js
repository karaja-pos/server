const UsersController = require("./UsersController");
const BarangController = require("./BarangController");
const KaryawanController = require("./KaryawanController");
const KategoriController = require("./KategoriController");
const MerekController = require("./MerekController");
const PosisiController = require("./PosisiController");
const TokoController = require("./TokoController");
const SuplierController = require("./SuplierController");
const SatuanController = require("./SatuanController");
const KrediturController = require("./KrediturController");
const GudangController = require("./GudangController");
const KasirController = require("./KasirController");
const InfoController = require("./InfoController");
const HistoryController = require("./HistoryController");
const TokenController = require("./TokenController");
const LaporanController = require("./LaporanController");
const PenarikanController = require("./PenarikanController");
const FileController = require("./FileController");

module.exports = {
  UsersController,
  BarangController,
  KaryawanController,
  KategoriController,
  MerekController,
  PosisiController,
  TokoController,
  SuplierController,
  SatuanController,
  KrediturController,
  GudangController,
  KasirController,
  InfoController,
  HistoryController,
  TokenController,
  LaporanController,
  PenarikanController,
  FileController,
};
