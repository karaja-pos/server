const consola = require("consola");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

// PEMBELIAN
const GetFakturPembelian = async (req, res, next) => {
  try {
    const data = await db(tableName.pembelian)
      .select(
        `${tableName.pembelian}.id`,
        `${tableName.pembelian}.no_faktur`,
        `${tableName.pembelian}.no_penerimaan`,
        `${tableName.pembelian}.tanggal_faktur`,
        `${tableName.pembelian}.tanggal_penerimaan`,
        `${tableName.pembelian}.jam_penerimaan`,
        `${tableName.pembelian}.barang_diterima`,
        `${tableName.pembelian}.barang_retur`,
        `${tableName.pembelian}.id_supplier`,
        `${tableName.suplier}.nama_supplier`,
        `${tableName.pembelian}.total_harga_pembelian`,
        `${tableName.pembelian}.id_toko`,
        // `${tableName.toko}.nama_toko`,
        `${tableName.pembelian}.telp`,
        `${tableName.pembelian}.user`
      )
      .join(
        tableName.suplier,
        `${tableName.pembelian}.id_supplier`,
        `${tableName.suplier}.id`
      );
    // .join(
    //   tableName.toko,
    //   `${tableName.pembelian}.id_toko`,
    //   `${tableName.toko}.id`
    // );

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const AddFakturPembelian = async (req, res, next) => {
  const { faktur, pembelian } = req.body;
  try {
    const jumlah = pembelian.reduce(
      (a, b) => (a = parseInt(a) + parseInt(b.jumlah)),
      0
    );
    const retur = pembelian.reduce(
      (a, b) => (a = parseInt(a) + parseInt(b.retur)),
      0
    );
    const total = pembelian.reduce(
      (a, b) => (a = parseInt(a) + parseInt(b.total)),
      0
    );
    const add = await db(tableName.pembelian).insert({
      no_faktur: faktur.no_fak,
      no_penerimaan: faktur.no_penerimaan,
      tanggal_faktur: faktur.tgl_fak,
      tanggal_penerimaan: faktur.tgl_penerimaan,
      jam_penerimaan: faktur.jam,
      barang_diterima: jumlah,
      barang_retur: retur,
      id_supplier: faktur.id_supplier,
      total_harga_pembelian: total,
      id_toko: 1,
      telp: "0823",
      user: "",
    });

    await pembelian.map((data) => {
      let total = parseInt(data.jumlah) - data.retur;
      db(tableName.barang)
        .select("id", "stok_akhir")
        .where({ id: data.kode })
        .then((barang) => {
          db(tableName.barang)
            .where({ id: barang[0].id })
            .update({ stok_akhir: barang[0].stok_akhir + total })
            .then((result) => {
              console.log(result);
            });
        });
    });

    const dataArr = pembelian.map((data) => {
      return {
        no_faktur: faktur.no_fak,
        id_barang: data.kode,
        barang_diterima: parseInt(data.jumlah),
        barang_retur: data.retur,
        harga_beli: parseInt(data.harga),
        jumlah_harga: data.total,
        user: "",
      };
    });
    await db(tableName.tampungPembelian).insert(dataArr);
    return WebResponse(res, 201, "Created", add);
  } catch (error) {
    return next(error);
  }
};

const GetFakturByNomor = async (req, res, next) => {
  const { nomor } = req.params;
  try {
    const data = await db(tableName.pembelian)
      .select(
        `${tableName.pembelian}.no_faktur`,
        `${tableName.pembelian}.no_penerimaan`,
        `${tableName.pembelian}.tanggal_faktur`,
        `${tableName.pembelian}.tanggal_penerimaan`,
        `${tableName.pembelian}.jam_penerimaan`,
        `${tableName.pembelian}.barang_diterima`,
        `${tableName.pembelian}.barang_retur`,
        `${tableName.pembelian}.id_supplier`,
        `${tableName.pembelian}.total_harga_pembelian`,
        `${tableName.pembelian}.id_toko`,
        `${tableName.pembelian}.telp`,
        `${tableName.pembelian}.user`,
        `${tableName.pembelian}.created_at`,
        `${tableName.suplier}.nama_supplier`
      )
      .join(
        tableName.suplier,
        `${tableName.pembelian}.id_supplier`,
        `${tableName.suplier}.id`
      )
      .where({ no_faktur: nomor });
    const tampung = await db(tableName.tampungPembelian)
      .select(
        `${tableName.tampungPembelian}.*`,
        `${tableName.barang}.nama_brg`,
        `${tableName.barang}.id_satuan`,
        `${tableName.satuan}.nama_satuan`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPembelian}.id_barang`,
        `${tableName.barang}.id`
      )
      .join(
        tableName.satuan,
        `${tableName.barang}.id_satuan`,
        `${tableName.satuan}.id`
      )
      .where({ no_faktur: nomor });
    const DATA = data.map((item) => {
      return {
        ...item,
        items: tampung,
      };
    });

    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

const DeleteFakturById = async (req, res, next) => {
  const { no_faktur } = req.params;
  try {
    const checkFaktur = await db(tableName.tampungPembelian)
      .select(
        `${tableName.tampungPembelian}.id_barang`,
        `${tableName.tampungPembelian}.barang_diterima`,
        `${tableName.tampungPembelian}.barang_retur`,
        `${tableName.barang}.stok_akhir`
      )
      .where({ no_faktur })
      .join(
        tableName.barang,
        `${tableName.tampungPembelian}.id_barang`,
        `${tableName.barang}.id`
      );

    const convertFak = await checkFaktur.map((data) => {
      let stok = data.barang_diterima - data.barang_retur;
      return {
        id: data.id_barang,
        stok_akhir: data.stok_akhir - stok,
      };
    });

    await convertFak.map((data) => {
      db(tableName.barang)
        .update({ stok_akhir: data.stok_akhir })
        .where({ id: data.id })
        .then((i) => {
          console.log(i);
        });
    });

    await db(tableName.tampungPembelian).where({ no_faktur }).del();
    await db(tableName.pembelian).where({ no_faktur }).del();

    return WebResponse(res, 200, "Deleted");
  } catch (error) {
    return next(error);
  }
};

// PENGIRIMAN
const GetPengiriman = async (req, res, next) => {
  try {
    const data = await db(tableName.pengiriman)
      .select(`${tableName.pengiriman}.*`, `${tableName.toko}.nama_toko`)
      .join(
        tableName.toko,
        `${tableName.pengiriman}.id_toko`,
        `${tableName.toko}.id`
      )
      .orderBy(`${tableName.pengiriman}.id`, "desc");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetPengirimanByToko = async (req, res, next) => {
  const { id_toko } = req.params;
  try {
    const data = await db(tableName.pengiriman)
      .where({ id_toko })
      .select(`${tableName.pengiriman}.*`, `${tableName.toko}.nama_toko`)
      .join(
        tableName.toko,
        `${tableName.pengiriman}.id_toko`,
        `${tableName.toko}.id`
      )
      .orderBy(`${tableName.pengiriman}.id`, "desc");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetPengirimanItems = async (req, res, next) => {
  const { no_pengiriman } = req.params;
  try {
    const data = await db(tableName.tampungPengiriman)
      .where({ no_pengiriman, status: "N" })
      .select(
        `${tableName.tampungPengiriman}.*`,
        `${tableName.barang}.id as id_barang`,
        `${tableName.barang}.nama_brg`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPengiriman}.id_barang`,
        `${tableName.barang}.id`
      );
    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const KirimBarang = async (req, res, next) => {
  const { data, barang } = req.body;
  try {
    const add = await db(tableName.pengiriman).insert({
      no_pengiriman: data.no_pengiriman,
      tgl_pengiriman: data.tgl_pengiriman,
      jam_pengiriman: data.jam_pengiriman,
      jumlah_item: data.jumlah_item,
      id_toko: data.id_toko,
      user: data.user,
    });

    const dataBarang = barang.map((item) => {
      return {
        no_pengiriman: data.no_pengiriman,
        id_barang: item.id,
        jumlah_item: item.jumlah,
        user: data.user,
      };
    });

    await db(tableName.tampungPengiriman).insert(dataBarang);
    req.io.sockets.emit("pengiriman", "pengiriman");
    return WebResponse(res, 201, "Created", add);
  } catch (error) {
    return next(error);
  }
};

const HapusPengiriman = async (req, res, next) => {
  const { id } = req.params;
  try {
    await db(tableName.pengiriman).where({ no_pengiriman: id }).del();
    await db(tableName.tampungPengiriman).where({ no_pengiriman: id }).del();
    req.io.sockets.emit("pengiriman", "pengiriman");
    return WebResponse(res, 200, "Deleted");
  } catch (error) {
    console.log(error);
    return next(error);
  }
};

const TerimaPengirimanByToko = async (req, res, next) => {
  const { id_toko } = req.params;
  const {
    id,
    id_barang,
    input_jumlah,
    input_retur,
    jumlah_diterima,
    jumlah_item,
    no_pengiriman,
  } = req.body;
  try {
    const trx = await db.transaction();
    const barang = await trx(tableName.barang)
      .select("stok_akhir")
      .where({ id: id_barang });
    const data = await trx(tableName.stok_toko)
      .select("retur", "stok_akhir")
      .where({ id_toko, id_barang });

    await trx(tableName.stok_toko)
      .where({ id_toko, id_barang })
      .update({
        stok_akhir: data[0].stok_akhir + input_jumlah,
        retur: data[0].retur + input_retur,
      });

    await trx(tableName.tampungPengiriman).where({ id }).update({
      retur: input_retur,
      jumlah_diterima: input_jumlah,
      status: "Y",
    });

    await trx(tableName.barang)
      .where({ id: id_barang })
      .update({ stok_akhir: barang[0].stok_akhir - jumlah_item });

    const dataTampung = await trx(tableName.tampungPengiriman)
      .where({ no_pengiriman, status: "N" })
      .count("id as hitung");

    if (dataTampung[0].hitung === 0) {
      await trx(tableName.pengiriman)
        .where({ no_pengiriman })
        .update({ status: "Y" });
    }

    if (trx.isCompleted) {
      trx.commit();
    } else {
      trx.rollback();
    }
    return WebResponse(res, 201, "Updated", trx.isCompleted);
  } catch (error) {
    return next(error);
  }
};

const CetakPengiriman = async (req, res, next) => {
  const { no_pengiriman } = req.params;
  try {
    const tampung = await db(tableName.tampungPengiriman)
      .where({ no_pengiriman })
      .select(
        `${tableName.tampungPengiriman}.*`,
        `${tableName.barang}.nama_brg`
      )
      .join(
        tableName.barang,
        `${tableName.tampungPengiriman}.id_barang`,
        `${tableName.barang}.id`
      );
    const data = await db(tableName.pengiriman)
      .where({ no_pengiriman })
      .select(`${tableName.pengiriman}.*`, `${tableName.toko}.nama_toko`)
      .join(
        tableName.toko,
        `${tableName.pengiriman}.id_toko`,
        `${tableName.toko}.id`
      );

    const dataArr = data.map((item) => {
      return {
        ...item,
        barang: tampung,
      };
    });

    return WebResponse(res, 200, "Success", dataArr);
  } catch (error) {
    return next(error);
  }
};

const GetHistoryPengiriman = async (req, res, next) => {
  const { id_toko, tgl_start, tgl_end, jenis } = req.query;
  try {
    const data = await db(tableName.pengiriman)
      .select(`${tableName.pengiriman}.*`, `${tableName.toko}.nama_toko`)
      .join(
        tableName.toko,
        `${tableName.pengiriman}.id_toko`,
        `${tableName.toko}.id`
      )
      .where({ id_toko })
      .where(`${tableName.pengiriman}.status`, jenis)
      .andWhereBetween("tgl_pengiriman", [tgl_start, tgl_end])
      .orderBy(`${tableName.pengiriman}.id`, "desc");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  // pembelian
  GetFakturPembelian,
  AddFakturPembelian,
  GetFakturByNomor,
  DeleteFakturById,
  // pengiriman
  GetPengiriman,
  GetPengirimanByToko,
  GetPengirimanItems,
  KirimBarang,
  HapusPengiriman,
  TerimaPengirimanByToko,
  CetakPengiriman,
  GetHistoryPengiriman,
};
