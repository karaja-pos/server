const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */
exports.up = async (knex) => {
  await knex.schema
    .createTable(tableName.masterAkun, (table) => {
      table.increments("id").notNullable();
      table.string("nama_akun").notNullable();
      table.string("session_user", 100).nullable();
    })
    .createTable(tableName.jenisAkun, (table) => {
      table.increments("id").notNullable();
      table.integer("id_akun").notNullable();
      table.string("nama_jenis_akun").notNullable();
      table.string("keterangan").nullable();
      table.string("session_user", 100).nullable();
    })
    .createTable(tableName.kasKeluar, (table) => {
      table.increments("id").notNullable();
      table.date("tanggal").notNullable();
      table.time("jam").notNullable();
      table.integer("id_akun").notNullable();
      table.integer("id_jenis_akun").notNullable();
      table.integer("jumlah_rupiah").defaultTo(0).notNullable();
      table.string("keterangan").nullable();
      table.string("yang_menerima").notNullable();
      table
        .enum("status", [1, 2, 3, 4])
        .defaultTo(1)
        .notNullable()
        .comment("1= pengajuan 2= disetujui 3= ditolak, 4= selesai");
      table.string("session_user", 100).nullable();
    })
    .createTable(tableName.kasMasuk, (table) => {
      table.increments("id").notNullable();
      table.date("tanggal").notNullable();
      table.time("jam").notNullable();
      table.integer("id_akun").notNullable();
      table.integer("id_jenis_akun").notNullable();
      table.integer("id_toko").notNullable();
      table.integer("jumlah").defaultTo(0).notNullable();
      table.string("diterima_dari").notNullable();
      table.string("keterangan").nullable();
      table.string("session_user", 100).nullable();
    });
};

/**
 *
 * @param {Knex} knex
 */
exports.down = async (knex) => {
  await knex.schema
    .dropTable(tableName.masterAkun)
    .dropTable(tableName.jenisAkun)
    .dropTable(tableName.kasKeluar)
    .dropTable(tableName.kasMasuk);
};
