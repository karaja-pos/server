const Knex = require("knex");
const tableName = require("../constant/tableName");
/**
 *
 * @param {Knex} knex
 */

exports.up = async (knex) => {
  await knex.schema
    .createTable(tableName.file, (table) => {
      table.increments("id").notNullable();
      table.string("title", 255).nullable();
      table.string("type", 50).nullable();
      table.specificType("file", "LONGBLOB").nullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").notNullable();
      table.string("user").nullable();
    })
    .createTable(tableName.barang, (table) => {
      table.increments("id").notNullable();
      table.integer("id_kategori").notNullable();
      table.string("kode_brg", 50).notNullable();
      table.string("id_merek", 50).notNullable();
      table.string("nama_brg").notNullable();
      table.text("deskripsi").notNullable();
      table.integer("stok_akhir").defaultTo(0).notNullable();
      table.integer("id_satuan").nullable();
      table.text("gambar_brg").nullable();
      // table.integer("gambar_brg").nullable();
      table.string("user", 150).nullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.satuan, (table) => {
      table.increments("id").notNullable();
      table.string("nama_satuan", 50).notNullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").notNullable();
      table.string("user").nullable();
    })
    .createTable(tableName.kreditur, (table) => {
      table.increments("id").notNullable();
      table.string("kode", 100).nullable();
      table.string("nama_kreditur", 150).nullable();
      table.text("alamat").nullable();
      table.string("telp", 20).nullable();
      table.string("user", 150).notNullable();
    })
    .createTable(tableName.karyawan, (table) => {
      table.increments("id").notNullable();
      table.string("kode_karyawan", 100).notNullable();
      table.text("password").notNullable();
      table.string("nama_karyawan", 150).notNullable();
      table.string("nomor_hp", 150).nullable();
      table.string("gambar_karyawan").nullable();
      table.integer("id_gambar").nullable();
      table.integer("id_posisi").nullable();
      table.integer("id_toko").nullable();
      table
        .enum("status", ["Y", "N"])
        .defaultTo("N")
        .notNullable()
        .comment("Y=Online N=Offline");
      table.enum("blokir", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.kategori, (table) => {
      table.increments("id").notNullable();
      table.string("kategori", 100).notNullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.merek, (table) => {
      table.increments("id").notNullable();
      table.string("merek", 150).notNullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.posisi, (table) => {
      table.increments("id").notNullable();
      table.string("posisi", 100).notNullable();
      table.enum("blokir", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.suplier, (table) => {
      table.increments("id").notNullable();
      table.string("kode_supplier", 50).notNullable();
      table.string("nama_supplier", 150).notNullable();
      table.string("telp_supplier", 20).notNullable();
      table.string("alamat_supplier").notNullable();
      table.enum("blokir", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.toko, (table) => {
      table.increments("id").notNullable();
      table.string("kode_toko", 50).notNullable();
      table.string("nama_toko", 150).notNullable();
      table.string("alamat").notNullable();
      table.string("telp", 20).notNullable();
      table.integer("pajak").defaultTo(0).notNullable();
      table
        .enum("status", ["Y", "N"])
        .nullable()
        .defaultTo("N")
        .comment("Y=Buka N=Tutup");
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })

    .createTable(tableName.pembelian, (table) => {
      table.increments("id").notNullable();
      table.string("no_faktur", 100).notNullable();
      table.string("no_penerimaan", 100).nullable();
      table.timestamp("tanggal_faktur").notNullable();
      table.timestamp("tanggal_penerimaan").notNullable();
      table.time("jam_penerimaan").notNullable();
      table.integer("barang_diterima").notNullable();
      table.integer("barang_retur").notNullable();
      table.integer("id_supplier").notNullable();
      table.integer("total_harga_pembelian").notNullable();
      table.integer("id_toko").notNullable();
      table.string("telp", 20).notNullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.tampungPembelian, (table) => {
      table.increments("id").notNullable();
      table.string("no_faktur", 100).notNullable();
      table.integer("id_barang").notNullable();
      table.integer("barang_diterima").notNullable();
      table.integer("barang_retur").notNullable();
      table.integer("harga_beli").notNullable();
      table.integer("jumlah_harga").notNullable();
      table.string("user", 150).notNullable();
    })
    .createTable(tableName.stok_toko, (table) => {
      table.increments("id").notNullable();
      table.integer("id_barang").nullable();
      table.integer("id_toko").nullable();
      table.integer("harga_jual").defaultTo(0).notNullable();
      table.integer("stok_akhir").defaultTo(0).notNullable();
      table.integer("retur").defaultTo(0).notNullable();
      table.float("diskon").defaultTo(0).nullable();
      table.string("user", 150).notNullable();
    })
    .createTable(tableName.users, (table) => {
      table.uuid("uuid").unique().primary().defaultTo(knex.raw("(UUID())"));
      table.string("email", 100).notNullable();
      table.string("username", 100).notNullable();
      table.text("password").notNullable();
      table.enum("role", ["admin", "staf", "toko", "user"]).notNullable();
      table.integer("id_pengguna", 100).nullable();
      table.integer("status", 150).defaultTo(1).unsigned().notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.pengiriman, (table) => {
      table.increments("id").notNullable();
      table.string("no_pengiriman").notNullable();
      table.date("tgl_pengiriman").notNullable();
      table.time("jam_pengiriman").notNullable();
      table.integer("jumlah_item").defaultTo(0).notNullable();
      // table.integer("total_harga").defaultTo(0).notNullable();
      table.integer("id_toko").nullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.tampungPengiriman, (table) => {
      table.increments("id").notNullable();
      table.string("no_pengiriman").notNullable();
      table.integer("id_barang").notNullable();
      table.integer("jumlah_item").defaultTo(0).notNullable();
      table.integer("retur").defaultTo(0).notNullable();
      table.integer("jumlah_diterima").defaultTo(0).notNullable();
      table.enum("status", ["Y", "N"]).defaultTo("N").nullable();
      table.string("user", 150).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.penjualanTunai, (table) => {
      table.increments("id").notNullable();
      table.string("no_penjualan").notNullable();
      table.date("tgl_penjualan").notNullable();
      table.time("jam_penjualan").notNullable();
      table.integer("total_item").defaultTo(0).notNullable();
      table
        .integer("sub_total")
        .comment("Harga sebelum diskon")
        .defaultTo(0)
        .notNullable();
      table.integer("total_harga").defaultTo(0).notNullable();
      table.integer("total_diskon").defaultTo(0).notNullable();
      table.integer("pajak").defaultTo(0).notNullable();
      table.string("rek_tujuan", 25).nullable();
      table.string("rek_pengirim", 25).nullable();
      table.date("tgl_transfer").nullable();
      table.time("jam_transfer").nullable();
      table.string("nama_pengirim", 25).nullable();
      table
        .integer("jenis_bayar")
        .defaultTo(1)
        .notNullable()
        .comment("1=Tunai, 2=NonTunai");
      table.integer("id_toko").notNullable();
      table.integer("session_kasir").notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.tampungPenjualanTunai, (table) => {
      table.increments("id").notNullable();
      table.string("no_penjualan").notNullable();
      table.integer("id_barang").notNullable();
      table.integer("id_satuan").notNullable();
      table.integer("jumlah_item").defaultTo(0).notNullable();
      table.integer("harga_satuan").defaultTo(0).notNullable();
      table.integer("diskon").defaultTo(0).notNullable();
      table.integer("harga_diskon").defaultTo(0).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.penjualanKredit, (table) => {
      table.increments("id").notNullable();
      table.string("no_penjualan").notNullable();
      table.date("tgl_penjualan").notNullable();
      table.time("jam_penjualan").notNullable();
      table.integer("total_item").defaultTo(0).notNullable();
      table
        .integer("sub_total")
        .comment("Harga sebelum diskon")
        .defaultTo(0)
        .notNullable();
      table.integer("total_harga").defaultTo(0).notNullable();
      table.integer("total_diskon").defaultTo(0).notNullable();
      table.integer("pajak").defaultTo(0).notNullable();
      table.integer("id_toko").notNullable();
      table.integer("id_kreditur").notNullable();
      table.string("nama_pembeli").nullable();
      table.string("nik").nullable();
      table.integer("dp").defaultTo(0).nullable();
      table.integer("session_kasir").notNullable();
      table.date("tanggal_pelunasan").nullable();
      table.time("jam_pelunasan").nullable();
      table.integer("session_lunas").nullable();
      table
        .string("status")
        .defaultTo("Y")
        .nullable()
        .comment("Y=Kredit aktif, N=Kredit telah dilunasi");
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.tampungPenjualanKredit, (table) => {
      table.increments("id").notNullable();
      table.string("no_penjualan").notNullable();
      table.integer("id_barang").notNullable();
      table.integer("id_satuan").notNullable();
      table.integer("jumlah_item").defaultTo(0).notNullable();
      table.integer("harga_satuan").defaultTo(0).notNullable();
      table.integer("diskon").defaultTo(0).notNullable();
      table.integer("harga_diskon").defaultTo(0).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.tampungBonus, (table) => {
      table.increments("id").notNullable();
      table.string("no_penjualan").notNullable();
      table.integer("id_barang").notNullable();
      table.integer("id_satuan").notNullable();
      table.integer("jumlah_item").defaultTo(0).notNullable();
      table.integer("harga_satuan").defaultTo(0).notNullable();
      table.integer("diskon").defaultTo(0).notNullable();
      table.integer("harga_diskon").defaultTo(0).notNullable();
      table.timestamp("created_at").defaultTo(knex.fn.now());
      table.timestamp("updated_at").defaultTo(knex.fn.now());
    })
    .createTable(tableName.token, (table) => {
      table.increments("id").notNullable();
      table.string("token").notNullable();
      table.integer("id_toko").notNullable();
    })
    .createTable(tableName.penarikan, (table) => {
      table.increments("id").notNullable();
      table.integer("toko_asal").notNullable();
      table.integer("toko_tujuan").notNullable();
      table.integer("id_barang").notNullable();
      table.integer("jumlah_item").notNullable();
      table.date("tgl_penarikan").notNullable();
      table
        .integer("status")
        .defaultTo(1)
        .comment("1=Prosses, 2=Confirmasi")
        .notNullable();
      table
        .integer("baca")
        .defaultTo(0)
        .notNullable()
        .comment("0=belum dibaca, 1=dibaca");

      table.integer("user").notNullable();
    });
};

/**
 *
 * @param {Knex} knex
 */

exports.down = async (knex) => {
  await knex.schema
    .dropTable(tableName.file)
    .dropTable(tableName.users)
    .dropTable(tableName.barang)
    .dropTable(tableName.satuan)
    .dropTable(tableName.kreditur)
    .dropTable(tableName.karyawan)
    .dropTable(tableName.kategori)
    .dropTable(tableName.merek)
    .dropTable(tableName.pembelian)
    .dropTable(tableName.posisi)
    .dropTable(tableName.suplier)
    .dropTable(tableName.tampungPembelian)
    .dropTable(tableName.stok_toko)
    .dropTable(tableName.toko)
    .dropTable(tableName.pengiriman)
    .dropTable(tableName.tampungPengiriman)
    .dropTable(tableName.penjualanTunai)
    .dropTable(tableName.tampungPenjualanTunai)
    .dropTable(tableName.penjualanKredit)
    .dropTable(tableName.tampungPenjualanKredit)
    .dropTable(tableName.token);
};
