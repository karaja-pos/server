const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.kreditur)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.kreditur).insert([
        {
          id: 1,
          kode: "KR0001",
          nama_kreditur: "Kredit Plus Cab. Limboto",
          alamat: "Limboto",
          telp: "0876373",
          user: "",
        },
        {
          id: 2,
          kode: "KR0002",
          nama_kreditur: "Kredit Plus Cab. Kwandang",
          alamat: "Kwandang",
          telp: "0876373",
          user: "",
        },
        {
          id: 3,
          kode: "KR0003",
          nama_kreditur: "Kredit Plus Cab. Kota Gorontalo",
          alamat: "Kota Gorontalo",
          telp: "0876373",
          user: "",
        },
        {
          id: 4,
          kode: "KR0004",
          nama_kreditur: "Kredit Plus Cab. Tilamuta",
          alamat: "Tilamuta",
          telp: "0876373",
          user: "",
        },
        {
          id: 5,
          kode: "KR0005",
          nama_kreditur: "Adira Cab. Limboto",
          alamat: "Limboto",
          telp: "0876373",
          user: "",
        },
        {
          id: 6,
          kode: "KR0006",
          nama_kreditur: "Adira Cab. Kota Gorontalo",
          alamat: "Kota Gorontalo",
          telp: "0876373",
          user: "",
        },
        {
          id: 7,
          kode: "KR0007",
          nama_kreditur: "Adira Cab. Tilamuta",
          alamat: "Tilamuta",
          telp: "0876373",
          user: "",
        },
        {
          id: 8,
          kode: "KR0008",
          nama_kreditur: "Adira Cab. Marisa",
          alamat: "Marisa Kab. Pohuwato",
          telp: "0876373",
          user: "",
        },
        {
          id: 9,
          kode: "KR0009",
          nama_kreditur: "Kredit Plus Cab. Popayato",
          alamat: "Popayato Kab. Pohuwato",
          telp: "0876373",
          user: "",
        },
      ]);
    });
};
