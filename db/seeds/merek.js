const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.merek)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.merek).insert([
        {
          id: 1,
          merek: "Samsung",
          user: "",
        },
        {
          id: 2,
          merek: "Oppo",
          user: "",
        },
        {
          id: 3,
          merek: "Advan",
          user: "",
        },
        {
          id: 4,
          merek: "Aldo",
          user: "",
        },
        {
          id: 5,
          merek: "Xiomi",
          user: "",
        },
        {
          id: 6,
          merek: "Infinix",
          user: "",
        },
        {
          id: 7,
          merek: "Vivo",
          user: "",
        },
        {
          id: 8,
          merek: "Realmi",
          user: "",
        },
        {
          id: 9,
          merek: "Nokia",
          user: "",
        },
        {
          id: 10,
          merek: "Bara",
          user: "",
        },
        {
          id: 11,
          merek: "Iphone",
          user: "",
        },
        {
          id: 12,
          merek: "Foome",
          user: "",
        },
        {
          id: 13,
          merek: "Robot",
          user: "",
        },
        {
          id: 14,
          merek: "Vivan",
          user: "",
        },
        {
          id: 15,
          merek: "Gaming",
          user: "",
        },
        {
          id: 16,
          merek: "Extra Bass",
          user: "",
        },
        {
          id: 17,
          merek: "Warna",
          user: "",
        },
        {
          id: 18,
          merek: "Memori Lainnya",
          user: "",
        },
        {
          id: 20,
          merek: "Stonik",
          user: "",
        },
        {
          id: 21,
          merek: "Blitz",
          user: "",
        },
        {
          id: 22,
          merek: "Fleco",
          user: "",
        },
        {
          id: 23,
          merek: "Panda",
          user: "",
        },
        {
          id: 24,
          merek: "Better",
          user: "",
        },
      ]);
    });
};
